Estándares de programación web
==============================

Presentación
------------

La web ha crecido de manera exponencial, influyendo sobre múltiples aspectos de
nuestra vida, sin embargo al nacer como una red descentralizada, la misma
siempre fue un espacio creativo, donde se forzaron los límites y las reglas.

En medio de ese proceso, una serie de instituciones intenta mantener el orden
creando una serie de [estándares][1] que permiten la interconexión y el
desarrollo de las herramientas necesarias el funcionamiento de la web.

Estos estándares, en la mayoría de los casos, se basan en prácticas que se
muestran útiles, se vuelven comunes y al fin necesarias.

 [1]: https://es.wikipedia.org/wiki/Estándares_web

Fundamentación
--------------

Debido en parte a que los estándares en la web salen de la práctica, su rápido
desarrollo y que en general comienzan a utilizarse antes de estandarizarse es
importante, no sólo conocer los actuales, sino también cuáles son los que se
están formando y cuáles son los problemas que se están solucionando.

También es importante conocer los mecanismos por los cuales se logra el ritmo
de cambio, utilizando _parches_, que permiten utilizar una tecnología, cuando
todavía no está disponible.

Objetivos
---------

El objetivo es que los alumnos conozcan algunos de los más importantes
estándares web en su versión actual (HTML5, css3 y javascript 5, AJAX, HTTP) y
también que puedan conocer cómo se están implementando actualmente nuevos
estándares mediante polyfills, mediante el uso de RIOT que implementa el
concepto de **componente web**.

Cronograma de actividades
-------------------------

### Diseño de una página web estática (1 semana)

Se aplicará los conceptos de *web semántica* en la creación de un sitio "sobre
mí" que muestre información del alumno, descripción, intereses, estudios,
experiencias. Debe incluir un menú para las distintas secciones. Dentro de la
descripción incluir una foto de perfil. Organizar el contenido basado en la web
semántica. Dicha web se irá mejorando durante el cursado y va a servir de
base para las siguientes actividades.

### Accesibilidad (1 semana)

En esta actividad optativa, se estudiaran las pautas de accesibilidad web y se
las utilizara en la página realizada en la actividad anterior.

### Aplicar estilos (2 semanas)

Se aplicarían estilos con bootstrap y/o estilos (frameworks de css) semánticos.
Vamos a proponer 2 frameworks de css: pure css porque es simplista y eficiente
y bootstrap porque es popular. Los alumnos pueden buscar otros (hay miles). Una
vez definido el framework, le vamos a pedir que agreguen los estilos a la web.

### Agregar contenido embebido (2 semanas)

Se agregaran contenidos embebidos en la misma web, mapas, videos, gráficos y
se estudiaran distintas formas de incrustar contenido externo.

### Agregar un componente web (3 semanas)

En esta actividad obligatoria, utilizando RIOT, se agregará un "componente web"
dinámico.

Equipo docente
--------------

Pablo Oldani y Eloy Espinaco trabajan como programadores desde hace varios
años, son entusiastas del software libre y trabajan principalmente en la web
desde hace más de 6 años.
