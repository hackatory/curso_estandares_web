# Diseño de una página web estática
Duracion: 1 semana
Teória:
- capitulo 1, 2 del libro HTML5
- https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/HTML_basics

## Qué es una página web?

Internet es una red de redes. En ella cada computadora se conecta a otra
computadora. Dentro de la red algunas computadoras se comportan como servidores
y otras como clientes. Cuando buscamos en internet usando un browser, firefox,
chrome, etc, osea cuando usamos la pc como cliente, le estamos pidiendo una
serie de documentos a otra computadora que está haciendo de servidor. Sin
importar la aplicación que estemos usando, facebook, google o el moodle, el
servidor sirve, manda una triada de archivos, html, css y js entre otros. Cada
archivo sirve con un propósito para el programa cliente o browser.

Cuando hacemos click sobre un enlace, el navegador hace una solicitud HTTP ^[en
general se utiliza el término en inglés 'request'] al servidor para obtener
otras páginas. HTTP, es un protocolo de transferencia que permite al cliente (o
sea el navegador) solicitar archivos y hacer pedidos al servidor.

El html dice cual es la estructura de la página, cual es el orden de los
elementos, si la información esta en tablas, listas o solo texto. el css nos
dice como es el formato, que colores, fuentes y tamaños se van a usar. Y por
último el js como es el comportamiento de la pagina, si se deberia mostrar un
mensaje de confirmación, animaciones o si algun elemento debe moverse dentro de
la página.

Estos archivos que envia el servidor, son archivos de texto plano, es decir que
no tienen otras propiedades como las que agregan los procesadores de texto como
el libre office. Para editarlos o crealos, tenemos que usar cualquier editor de
texto, gedit, vim o emacs.

## Como vamos a trabajar

Vamos a crear y mostrar una página web muy simple.

Abrí tu editor favorito y pegá el siguiente código

~~~ html
<!doctype html>
<html lang="es-AR">
<head>
  <meta charset="utf-8">
  <title>Primera Prueba</title>
</head>
<body>
  <p>Hola!</p>
</body>
</html>
~~~

_Nota: No te preocupes si no entendés este código lo vamos a ir viendo por
partes más adelante._

Salvá el archivo con el nombre "index.html" la extención es importante.

Ahora desde la consola, escribí el nombre de tu navegador seguido del path
hasta el archivo que acabamos de crear. Por ejemplo en mi caso seria

    firefox index.html

Cuando se abra el navegador, estarás viendo el archivo index.html que creamos.
ahora prueba cambiar el texto donde dice "Hola!" por algo distinto, salva el
archivo y recarga la página del navegador, verás tus cambios en pantalla.

## Actividades

Se aplicará los conceptos de web semántica en la creación de un sitio personal,
con información de contacto, experiencias e intereses.

La página debe tener distintas secciones y un menú para navegar entre las
mismas y también debe incluir una foto de perfil.

El contenido debe estar organizado utilizando las etiquetas semánticas
introducidas en el estándar web HTML5. Esta web la vamos a ir mejorando durante
el cursado y va a servir de base para las otras actividades.

1. Cuestionario
  Cuales de estos elementos pertenecen a HTML5?

~~~
    <css>, <section>, <aside>, <nav>
    <article>, <js>, <aside>, <nav>
    <article>, <section>, <firefox>, <nav>
    <article>, <section>, <aside>, <lang>
    <article>, <section>, <aside>, <nav>
~~~

  Cuales de estas etiquetas se usan en el head de un documento?

~~~
    <body>, <title>, <link>, <description>
    <meta>, <p>, <link>, <description>
    <meta>, <title>, <link>, <description>
    <meta>, <title>, <section>, <description>
~~~

  Cuales de estas etiquetas se usan en el body de un documento?

~~~
    <title>, <section>, <aside>, <nav>, <header>
    <p>, <section>, <aside>, <nav>, <header>
    <p>, <description>, <aside>, <nav>, <header>
    <p>, <section>, <meta>, <nav>, <header>
    <p>, <section>, <aside>, <nav>, <head>
~~~

2. Dada la siguiente página web, hacer los cambios que cree necesario para
   llevarla a la version HTML5

3. Crea un documento HTML5, usando las etiquetas, header, footer, aside y nav,
   donde se muestre tu información personal. Algunas de las cosas que podes
   mostrar son:

   * Nombre y apellido
   * Descripción
   * formas de contacto (email, telefono, direccion)
   * Tecnologías que te gusten
   * Experiencia (en que trabajaste)
   * gustos personales (peliculas, musica, helados)
   * links a redes sociales o blogs
   * una foto o un video
