Estilos en la web
=================

Las hojas de estilo son una cuestión central en el desarrollo web y es también
una de las problemáticas principales en las cuestiones de estandarización ya
que su implementación fue sumamente compleja por la falta de estándares claros
y por falta de soporte en los distintos navegadores.

Cómo se escriben los estilos en la web
--------------------------------------

Los estilos en la web se escriben utilizando `css` que es una sigla en inglés
que significa "hojas de estilo en cascada". La sintaxis de este lenguaje es
simple (aunque las hojas se pueden volver extremadamente complejas), cada hoja
consta de un conjunto de "reglas", cada regla consta de un "selector" (que
indica sobre que elementos se aplica la regla y un conjunto de "propiedades" y
"valores".

Veamos un ejemplo:

```css
p {
  color: blue;
  text-align: center;
}
```

En este ejemplo, el selector es `p` que selecciona todos los párrafos del
documento, y las dos declaraciones indican que el color del texto debe ser azul
y que el texto se debe alinear a la izquierda.

Cuáles son las ventajas de este sistema?
----------------------------------------

En un principio esta forma de describir los estilos puede resultar llamativo (e
incluso molesto) debido en gran medida a que estamos acostumbrados, por los
procesadores de texto, a ver los estilos y el contenido juntos. De esta forma
esta cuestión de los selectores puede resultar tediosa. Sin embargo, como
veremos a continuación, esta forma tiene una serie de ventajas que justifican
dicha complejidad.

Por un lado, esta separación entre contenido y formato, permite la
reutilización de las hojas de estilo en múltiples páginas, obteniendo así
estilos más coherentes y la posibilidad de cambiar los mismos desde un punto
central. Por otro lado, la estructura de múltiples reglas y selectores permite
aplicar distintas hojas de estilo sobre el mismo documento combinando los
resultados.

Referencias
-----------

Para consultar se puede utilizar la documentación en la
[MDN][3].

 [3]: https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/CSS_basics

Actividades
-----------

 1. El primer css (obligatorio)

   Lo que haremos será aplicar lo que se propone en la guía de [CSS básico][1] de
   la MDN a el archivo HTML que realizamos en la actividad anterior. Una vez
   realizado el cambio, para poder compartirlo, mostrarlo y pedir ayuda,
   utilizaremos [JSbin][2]. Una vez que comiencen a trabajar agreguen un hilo en
   el siguiente foro.

 2. Selectores (opcional)

   Dado el siguiente código HTML, indicar qué elemento tiene color rojo, etc.

   Escribir dos reglas de css, de forma que el texto del parrafo sea de color
   gris y los resaltados de color rojo.

 3. Modelo de caja (obligatorio)

 4. Display y position (opcional)

 5. Pull y clearfix (opcional)

 6. Pseudo selectores (opcional)

 7. Pseudo elementos (opcional)

 8. Tipografías (opcional)

 9. Transiciones (opcional)

 10. Modelo de caja (opcional)

 11. CSS frameworks (obligatorio)

   La posibilidad de reutilizar hojas de estilos en distintos sitios, permitió
   que se crearan cantidad de hojas de estilos que son usadas como plantillas
   en muchos sitios, para utilizarlos, se deben seguir ciertas estructuras y
   utilizar nombres de clases específicos al escribir el HTML, que ya están
   definidas en las hojas de estilo provistas.

   En esta actividad utilizaremos el framework [Bootstrap][4] para agregar
   estilos a nuestro sitio de ejemplo.

 [1]: https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/CSS_basics
 [2]: http://jsbin.com/?html,css,output
 [4]: http://www.bootstrap1.com/es/getting-started.html

 12. Personalizando el sitio (opcional)

   Por último, la posibilidad de utilizar múltiples hojas de estilo en el mismo
   sitio, nos permite utilizar un framework y nuestras hojas de estilo
   personales para darle un toque personal al sitio.

   Agregar una hoja de estilo personal al sitio web creado cambiando la
   tipografía y los colores.
