---
duracion: 1 semana
teória:
  - capitulo 3 del libro HTML5
  - Articulos del 1 al 4 inclusive de https://developer.mozilla.org/es/docs/Learn/HTML/Forms
---

Introducción
---

En este plan de trabajo, exploraremos los formularios web, los cuales son
particularmente complejos en HTML, pero a la vez son importantes ya que son el
principal mecanismo por el cual el el usuario puede interactuar con la página.

Hasta ahora, la única forma que estudiamos en que el usuario puede interactuar
es haciendo click en los distintos enlaces, pero esta forma de interacción es
muy limitada.

Los formularios sirven para incorporar en la página campos que permiten
construir pedidos específicos que son enviados al servidor para consultar datos
o para realizar cambios.

Veamos algunos ejemplos:

    <form action="/buscar">
      <label for="terminos_busqueda">Términos de búsqueda</label>
      <input id="terminos_busqueda" name="q" type="search" required>

      <input type="submit" value="buscar">
    </form>

En este formulario, el usuario puede ingresar términos que desea buscar dentro
del sitio, en realidad, lo que hace es construir una url con los datos
ingresados, o sea que funciona como un enlace.

En este caso, la url que se construye tiene la forma:
`url_actual.com/buscar?q=terminos`.

    <form action="/entrar" method="post">
      <label for="usuario">Nombre de usuario</label>
      <input id="usuario" name="username" type="text" required>

      <label for="contraseña">Contraseña</label>
      <input id="contraseña" name="password" type="password" required>

      <button>Entrar</button>
    </form>

En este caso, la url que se construye no incluye el nombre de usuario y la
contraseña, porque el método que se utiliza es "POST", por lo tanto el
navegador envía estos datos en la solicitud enviada al servidor.

Pueden probar distintos códigos y como son recibidos por el servidor en
https://www.w3schools.com/html/tryit.asp?filename=tryhtml_form_submit

---

Cuestionario (03-formularios.gift)

Ejercicio 4

Crear un formulario de registro en el cual el usuario ingrese:

 - Nombre, apellido y Fecha de nacimiento,

 - Email, pagina web personal y teléfono

 - Lugar de residencia

 - Una serie de radio inputs para indicar las preferencias con respecto a las
   publicidad (un mail semanal, mensual o ninguno)

 - Una imagen de perfil de tipo jpg o png.

 - Una casilla de verificación para aceptar los términos y condiciones.

 - Color favorito

 - Experiencia en el uso de software libre. (un valor entre 0 y 100)

 - Contraseña.

El formulario debe enviar los datos a
"https://www.w3schools.com/action_page.php" y debe tener un botón para
"resetear" el formulario. También debe validar la presencia de los campos más
importantes (dejamos esta cuestión a su criterio) y el formato de los mismos,
por ejemplo, verificar que la contraseña tenga entre 10 y 60 caracteres.
