Duracion 1 semana

Preguntas

1. dentro de que elemento se agrega el código de Javascript
script

2. cuál es la syntaxis correcta de una función en Javascript
 function:myFunction()
 function = myFunction()
 function myFunction()

3. cual será el resultado de cada sentencia

(true || 1 > 99 && "o".emty?)

true

4. que tipo tiene cada variable
[1, 2, "3"] object
{ nombre: 'pablo' } object
1221 number
"1121" string
undefined undefined

5. Cuas es la forma correcta de inicializar un Array
 var colors = (1:"red", 2:"green", 3:"blue")
 var colors = 1 = ("red"), 2 = ("green"), 3 = ("blue")
 var colors = ["red", "green", "blue"]
 var colors = "red", "green", "blue"

6. JavaScript y Java son el mismo lenguaje?.
true|false

7. dado el objeto persona, cual de estas opciones devuelven el valor booleano true?
var persona = { nombre: 'Liza', signo: 'Libra', estado: 'soltera', estudiando: true }

persona.nombre == 'Liza'
persona["signo"] != 'Virgo'
persona.estudiando && persona.estado == 'soltera'
persona.apellido === 'undefined'



Ejercicios:
1. Hola mundo, hacer una función, saludar, que reciba un nombre y devuelva el otro string de la forma
"Hola, nombre!"
Por ejemplo, si llamo a la funcion con saludar('pablo'), me tiene que devolver, "Hola, Pablo!".
Si no le paso ningun argumento, saludar(), tiene que devolver el string, "Hola, Mundo!"

Escribir el código de una función, paridad, a la que se pasa como parámetro
un número entero y devuelve como resultado una cadena de texto que indica si el
número es par o impar.
ejemplo:
paridad(1) => 'impar'
paridad(2) => 'par'

El cálculo de la letra del Documento Nacional de Identidad (DNI)
es un proceso matemático sencillo que se basa en obtener el resto de la
división entera del número de DNI y el número 23.  A partir del resto de la
división, se obtiene la letra seleccionándola dentro de un array de letras.  El
array de letras es:

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P',
'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T']

Por tanto si el resto de la división es 0 , la letra del DNI es la T y si el
resto es 3 la letra es la A .  Con estos datos, elaborar un pequeño script que:

Dado un DNI, hacer una función que devuelva:

1. se debe comprobar si el número es menor que 0 o mayor que 99999999 . Si no
   es el caso, se debe devolver el string "Error" en lugar de la letra.

2.  Si el número es válido, se calcula la letra que le corresponde según el
    método explicado anteriormente.


escribir una función que dada una lista de paices y una lista de personas como las siguientes

var paises = ['Argentina', 'Brasil', 'Chile', 'Paraguay', 'Uruguay', 'Bolivia']

var personas = [
  {
    nombre: 'Pablo',
    apellido: 'Perez',
    pais: 1
  },
  {
    nombre: 'Martin',
    apellido: 'Gonzalez',
    pais: 2
  }
  {
    nombre: 'Uma',
    apellido: 'Dosso',
    pais: 0
  }
]

devuelva una lista con sólo los nombres y los países reemplazados en la lista de países.

var resultado = [
  {
    nombre: 'Pablo',
    pais: 'Brasil'
  },
  {
    nombre: 'Martin',
    pais: 'Chile'
  }
  {
    nombre: 'Uma',
    pais: 'Argentina'
  }
]
