# Plan de trabajo 5

Duracion: 2 semanas

## JavaScript en el navegador

### Recursos

JavaScript es el lenguaje de programación de la web, en las páginas se pueden
agregar scripts para posibilitar interacciones distintas y novedosas en las
páginas, animaciones y scripts más complejos que permiten convertir una página
web en lo que llamamos una "aplicación web" que permite reemplazar una
aplicación de tradicional de escritorio.

Al ejecutar código javascript en el contexto de una página web, es posible
interactuar con el navegador, mediante la interacción con el objeto `window`
También es posible modificar la página en si, mediante la interacción con el
objeto `document` y es posible realizar peticiones asíncronas (o sea que no
interrumpen la navegación) e

[La trampa de JavaScript](https://www.gnu.org/philosophy/javascript-trap.es.html)

### Actividades

Cuestionario:



Contenidos


Manipulación del BOM
Manipulación del DOM

Bibliografía

Sección 3 del libro "Inspírate"

Cuestionario


Pedazo de código html, si 
