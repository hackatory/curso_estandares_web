<preview-comment>
  <style>
    label, input, textarea {
      display: block;
    }

    .preview {
      background-color: grey;
      width: 300px;
      min-height: 100px;
      border-radius: 15px;
    }

    .preview img {
      margin: 10px;
      border-radius: 50%;
      float: left;
    }
  </style>

  <h3>Nuevo Commentario</h3>
  <form>
    <label>Email
      <input type="email" name="email" ref="email" onChange={emailUpdate}>
    </label>
    <label>Comentario
      <textarea name="comment" ref='comment' onKeyPress={commentUpdate}></textarea>
    </label>
    <input type="submit">
  </form>

  <article class='preview comment'>
    <img src="http://www.gravatar.com/avatar/{photo}.jpg" alt={email}/>
    <p>{comment}</p>
  </article>

  <script>
    emailUpdate(){
      this.email = this.refs.email.value
      this.photo = md5(this.email)
    }

    commentUpdate(){
      this.comment = this.refs.comment.value
    }

  </script>
</preview-comment>
