<questionarie-form>

  <style>
    .incorrect {
      background-color: #f02d2d;
      margin: 22px;
      padding: 11px;
      text-align: center;
      color: #111;
    }

    .correct {
      font-size: 1.5em;
      background-color: #62f962;;
      margin: 22px;
      padding: 11px;
      text-align: center;
      color: #111;
    }

    .actions {
      margin-top: 1em;
      display: flex;
      justify-content: center;
    }

    h1 {
      font-size: 2em;
    }
  </style>

  <div show={state === 'incorrect'} class="incorrect">
    Tu respuesta no es correcta. Intenta de nuevo
  </div>

  <div show={state === 'correct'}>
    <p class='correct'>Correcto!</p>
  </div>

  <div>
    <div each={question, question_index in opts.questions}>
      <h1>{question.name}</h1>

      <div each={option, index in question.options }>
        <input
          type="radio"
          name={"question" + question_index}
          id={'option' + index}
          index={index}
        />
        <label for="{'option' + index}">{option}</label>
      </div>
    </div>

    <div show={state !=='correct'} class='actions'>
      <button onClick={submit}>Enviar</button>
    </div>
  </div>

  <div show={state === 'correct'}>
    <a href={opts.action}>Siguiente</a>
  </div>

  <script>

  var self = this

  submit(){
    self.state = 'correct'

    opts.questions.forEach(function(element, index) {
      var selected = 'input[name="question' + index + '"]:checked'
      var responded = document.querySelector(selected).attributes.index.value
      if(Number(responded) !== opts.questions[index].key){
        self.state = 'incorrect'
      }

      if(self.state !== 'incorrect'){
        self.state = 'correct'
      }

    });
  }

  </script>
</questionarie-form>
