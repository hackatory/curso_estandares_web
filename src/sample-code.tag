<sample-code>
  <style>
    #editor {
      display: flex;
      justify-content: space-between;
    }

    #source{
      border-color: #ada5a5;
      border-style: solid;
      border-width: 1px;
      margin: 1px;
      padding: 2px;
      width: 100%
    }

    iframe {
      border-color: #ada5a5;
      border-style: solid;
      border-width: 1px;
      margin: 1px;
      padding: 2px;
      width: 100%
    }
  </style>

  <template ref="original"><yield /></template>

  <div id='editor'>
    <div id='source' ref='source'></div>

    <iframe srcdoc="{ code }">
    </iframe>
  </div>

  <script>
    const self = this

    this.on('mount', function() {
      this.original_code = this.refs.original.content.textContent
      this.update({code: this.original_code})
      CodeMirror(this.refs.source, {
        mode: "text/html",
        lineWrapping: true,
        lineNumbers: true,
        styleActiveLine: true,
        matchBrackets: true,

        extraKeys: {
          "Ctrl-Space": "autocomplete"
        },
        value: this.code,
      }).on('change',function(cm){
        self.update({code: cm.getValue()})
      })
    })

    this.preview = function () {
      this.update({code: this.refs.source.value})
    }
  </script>
</sample-code>
