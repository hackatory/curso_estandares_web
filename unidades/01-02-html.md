# Qué es una etiqueta

Las etiquetas(tag) son marcas que se usan para señalar el inicio y el fin de un
elemento.

Todas las etiquetas comparten el mismo formato: empiezan con el signo _menor que_
"\<" y terminan con el signo _mayor que_ "\>".

Por lo general, hay dos tipos de etiquetas: la etiquetas de inicio, por
ejemplo \<html\>, y las etiquetas de cierre, por ejemplo \</html\>. La única
diferencia entre la etiqueta de inicio y la de cierre es la barra oblicua "/".
El contenido queda etiquetado al colocarlo entre ese par de etiquetas.

HTML trata esencialmente de elementos. Aprender HTML consiste en aprender a
usar diferentes etiquetas.

Por ejemplo:

~~~html
<title>Título del la página</title>
~~~

Es una etiqueta que se llama "title" y su contenido es "Título de la página".
Esta etiqueta en particular tiene como fin configurar el título que el usuario
verá en el tab o barra de la ventada cuando abra la página.

Las etiquetas pueden contener otras etiquetas dentro, es decir se pueden anidar.

Otro ejemplo podría ser:

~~~html
<p>Esto es <em>muy <strong>correcto</strong>!</em></p>
~~~

En este ejemplo todo se encuentra dentro de un párrafo dado que se usa la
etiqueta "p".  Dentro de éste hay una etiqueta "em", que configura el texto en
cursiva. Por último la palabra "correcto" está dentro de una etiqueta
"strong", que pone el texto en negrita.

Se ve de la siguiente forma:

<div>
  <p>Esto es <em>muy <strong>correcto</strong>!</em></p>
</div>

Cabe aclarar que no todas las etiquetas tienen una etiqueta de cierre. "br",
por ejemplo es un salto de linea que no se cierra

~~~html
una linea
<br>
otra linea
~~~

## Atributos

Para otorgar mayor significado a una etiqueta, se pueden usar atributos. Los
atributos se colocan dentro de la etiqueta de inicio y consisten de un nombre y
un valor separados por el signo igual "=". Tanto el nombre como el valor pueden
escribirse sin comillas si no se usan espacios en blanco o caracteres
especiales como:

~~~
" ' ` = <  >.`'
~~~

Ejemplos:

~~~html
<a href="/google.com">google</a>
~~~

La etiqueta "a" crea un link con el texto "google"

"href" es el nombre del atributo, y "/google.com" es su valor. Cada etiqueta
tiene una serie de atributos que acepta y cada uno tiene un uso en particular.
En este caso "href", es la url a la que el navegador va a ser redireccionado
cuando el usuario haga click en el link creado con la etiqueta "a"

El valor, junto con el signo "=", pueden ser omitidos si el valor es igual a una
cadena de texto vacía.

Ejemplo de etiquetas válidos

~~~html
<input name=address disabled>
<input name=address disabled="">

<input name=address maxlength=200>
<input name=address maxlength='200'>
<input name=address maxlength="200">
~~~

Son varios los atributos diferentes que se pueden aplicar a la mayoría de los
elementos.

Por ejemplo, con frecuencia se usan atributos en la etiqueta body, mientras que
raras veces se usa atributos en la etiqueta br, ya que un salto de línea es un
elemento simple, que no tiene parámetros que ajustar.

Al igual que existen muchos elementos, existen también muchos y diferentes
atributos. Algunos atributos están hechos a la medida para un elemento
concreto, mientras que otros se pueden usar para muchos elementos diferentes. Y
viceversa: algunos elementos sólo pueden contener un tipo de atributo mientras
otros pueden contener muchos.

Puede sonar algo confuso, pero una vez que se acostumbren a los diferentes
atributos, se vuelve de lo más lógico y pronto van a ver qué fácil es usarlos
y cuántas posibilidades brindan.

## Formato de una etiqueta de HTML5

~~~html
<etiqueta atributo="valor">
  texto de la etiqueta
</etiqueta>
~~~

[Siguiente](01-02-html-preguntas.html)
