# Cómo es un documento de HTML

Éstos son los elementos básicos que toda página HTML debe llevar:

Lo primero es el DOCTYPE que informa al navegador que el contenido siguiente se
trata de un archivo HTML (todos los navegadores modernos analizan la presencia
del DOCTYPE)

Luego continúa con la marca:\<html\> y finaliza con la marca:\</html\> al final del
archivo. En todo documento HTML, su elemento raíz o nodo superior siempre es la
etiqueta "\<html\>".

Un documento de HTML es un árbol de etiquetas. es decir que es un conjunto de
etiquetas que se van anidando entre ellas. Tiene dos secciones muy bien
definidas que son la cabecera:

~~~html
<head> </head>
~~~

Y el cuerpo de la página:

~~~html
<body> Cuerpo de la página. </body>
~~~

El elemento head contiene los metadatos que aportan información extra sobre la
página, como su descripción, autor, o título que deberá hacer referencia al
contenido en sí de la página. Además, puede incluir referencias externas a
contenidos necesarios para que el documento se muestre y comporte de manera
correcta (como hojas de estilos o scripts). Sólo puede existir un head.  El
título debe hacer referencia al contenido en sí de la página.

Dentro de la cabecera disponemos la etiqueta meta donde definimos la propiedad
charset con el valor UTF-8 que es un formato de caracteres ampliamente empleado
en internet (si no disponemos este formato no podemos disponer caracteres
acentuados por ejemplo, más adelante veremos en profundidad la etiqueta meta):

~~~html
<meta charset="UTF-8">
~~~

Todo el texto que dispongamos dentro del <body> aparece dentro del navegador
tal cual lo hayamos escrito.

Todas las páginas tienen como mínimo esta estructura: cabecera y cuerpo (head y
body).

Otra cosa importante es que el lenguaje HTML no es sensible a mayúsculas y
minúsculas, es decir podemos escribirlo como más nos guste, además no requiere
que dispongamos cada marca en una línea (podríamos inclusive escribir toda la
página en una sola línea!; cosa que no conviene ya que somos nosotros quienes
tendremos que modificarla en algún momento).

Un documento de HTML básico, se ve de la siguiente forma.

~~~html
<!DOCTYPE html>
<html>
  <head>
    <title>Título del la página</title>
  </head>
  <body>
    <h1>Un título</h1>
    <p>Esto es un <a href="demo.html">simple</a> ejemplo.</p>
    <!-- esto es un comentario -->
  </body>
</html>
~~~

[Siguiente](01-04-html.html)
