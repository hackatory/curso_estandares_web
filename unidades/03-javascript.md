# Javascript

Javascript es un lenguaje de programación creado por Netscape (uno de los
primeros navegadores cuyo código termino convirtiendose en firefox). Este
lenguaje es interpretado por el navegador y permite agregar otra forma de
contenido en las páginas web.

Veamos por ejemplo este botón:

~~~ html
<button onclick="alert('hiciste click')">Hace click acá</button>
~~~

<button onclick="alert('hiciste click')">Hace click acá</button>

Al hacer click en el botón, el navegador ejecuta el _script_ escrito en el
atributo `onclick` que en este caso muestra una alerta, agregando así
_interactividad_ a la página web.

Este no es el modo en que se escriben los scripts en el _mundo real_, en
general, al igual que con las hojas de estilo, es común separar los scripts del
resto de la página, para permitir trabajar con el mismo más comodamente, poder
reutilizarlo, etc.

~~~ { data-is="sample-code" }
<button id="ejemplo">Hace click acá</button>
<script>
 let boton = document.getElementById('ejemplo')

 boton.addEventListener('click', function () {
   alert('hiciste click')
 })
</script>
~~~

<link rel="stylesheet" href="http://esironal.github.io/cmtouch/lib/codemirror.css">
<link rel="stylesheet" href="http://esironal.github.io/cmtouch/addon/hint/show-hint.css">
<script src="http://esironal.github.io/cmtouch/lib/codemirror.js"></script>
<script src="http://esironal.github.io/cmtouch/addon/hint/show-hint.js"></script>
<script src="http://esironal.github.io/cmtouch/addon/hint/xml-hint.js"></script>
<script src="http://esironal.github.io/cmtouch/addon/hint/html-hint.js"></script>
<script src="http://esironal.github.io/cmtouch/mode/xml/xml.js"></script>
<script src="http://esironal.github.io/cmtouch/mode/javascript/javascript.js"></script>
<script src="http://esironal.github.io/cmtouch/mode/css/css.js"></script>
<script src="http://esironal.github.io/cmtouch/mode/htmlmixed/htmlmixed.js"></script>
<script src="http://esironal.github.io/cmtouch/addon/selection/active-line.js"></script>
<script src="http://esironal.github.io/cmtouch/addon/edit/matchbrackets.js"></script>

<style type="text/css">
.CodeMirror {
  font-size: 15px;
  width: 100%;
  height: 100%;
}
</style>

<script type="riot/tag" src="sample-code.tag"></script>
<script src="https://cdn.jsdelivr.net/npm/riot@3.8/riot+compiler.min.js"></script>
<script>riot.mount('sample-code')</script>
