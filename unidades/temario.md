Curso HTML + CSS

HTML parte 1

1. Que es una etiqueta
  - Introduccion
  - Atributos
  - Formato de una etiqueta
2. El documento HTML
  - Formato
  - Las etiquetas principales
    - html
    - head
    - body
3. Etiquetas de formato de texto
  - b
  - i
  - u
  - strike
  - strong
  - tt
  - sup
  - sub
  - ins
  - del
  - big
  - small
4. Enlaces
5. Listas
6. Las nuevas etiquetas de HTML5
  - header
  - nav
  - footer
  - aside
  - article
  - section

CSS

5. Introduccion a CSS
  - Introduccion
  - formato basico
6. Incluir estilos en un documento
  - inline
  - en un tag
  - en un archivo externo.
7. Propiedades
  - Colores
  - Fuente
  - formatos de texto
8. selectores y pseudo clases
9. Modelo de cajas
10. Margin, padding y border.

Formularios HTML parte 2.
